Torque-automation
1- ensure full reachability between all nodes and resolving hostname
2- disable selinux and firewall


ansible-palybook main.yml

if you want to install pbs_mom in a node make sure to start trqauthd service
and update the $TORQUE_HOME/server_priv/nodes

to test after installation:
qmgr -c 'p s'
qstat -q
pbsnodes -a

su - username
echo "sleep 30" | qsub

qstat
